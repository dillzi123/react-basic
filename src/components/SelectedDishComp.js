import React, {Component} from 'react';
import { Loading } from './LoadingComponent';
import { Card, CardImg,CardText, CardBody, CardTitle, ListGroup, ListGroupItemText, Breadcrumb, BreadcrumbItem,
         Modal, ModalHeader, ModalBody, Button, Label, Row} from 'reactstrap';
import { baseUrl } from '../shared/baseUrl';
import {Control, LocalForm, Errors} from 'react-redux-form';         
import {Link} from 'react-router-dom';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';


//validation
 const required = (val)=> val && val.length;
 const maxLength  = (len)=> (val)=> !val || (val.length<=len);
 const minLength = (len)=> (val)=> val && (val.length>=len);

//Comment Form Class Start
 class CommentForm extends Component
  {
      constructor(props)
      {
          super(props);
          this.state = {
              isModalOpen: false
          };
      }
      
     toggleModal = ()=>{
         this.setState({isModalOpen : !this.state.isModalOpen})
     }
     //Alert after submit
     handleSubmit(values)
     {
        this.toggleModal();
        this.props.postComment(this.props.dishId, values.rating, values.name, values.comment);

         
     }

      render()
      {
          return(
              <React.Fragment>
                  {/* Modal start */}
              <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}> 
                  <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                  <ModalBody>

                      {/* Form start */}
                      <LocalForm onSubmit={(values)=>this.handleSubmit(values)}>
                      <Row className='form-group mx-1'>
                          <Label htmlFor='rating'>Rating</Label>
                           <Control.select model='.rating' id='rating' name='rating' className='form-control'>
                               <option>1</option>
                               <option>2</option>
                               <option>3</option>
                               <option>4</option>
                               <option>5</option>
                           </Control.select>
                        </Row>
                        <Row className='form-group mx-1'>
                          <Label htmlFor='name'>Your Name</Label>
                           <Control.text model='.name' id='name' name='name' placeholder='Your Name' className='form-control'
                             validators={
                                 {
                                     required, minLength: minLength(3), maxLength: maxLength(15)
                                 }
                             } />
                            <Errors className='text-danger' model='.name' show='touched'
                            messages={
                                {
                                    required:'',
                                    minLength :'Must be greater than 2 characters',
                                    maxLength: 'Must be 15 characters or less'
                                }
                            }
                                />  
                        
                        </Row>

                        <Row className='form-group mx-1'>
                          <Label htmlFor='comment'>Comment</Label>
                           <Control.textarea model='.comment' id='comment' name='comment' rows='6'className='form-control'  />
                        </Row>
                        <Button type='submit' color='primary'>Submit</Button>
                       </LocalForm>
                       {/* form end */}

                  </ModalBody>
              </Modal>
              {/* Modal end */}

           {/* // Comment button that will be visible on render comment function */}
              <Button outline onClick={this.toggleModal}><i class="fa fa-pencil" aria-hidden="true"></i> Submit Comment</Button>
 
             </React.Fragment> 
          )
      }

  }

  //Comment Form Class End

     
    
   function RenderDish({dish, isLoading, errMess})
    {   
         if (isLoading) {
        return(
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
        );
    }
    else if (errMess) {
        return(
            <div className="container">
                <div className="row">            
                    <h4>{errMess}</h4>
                </div>
            </div>
        );
    }
        else if(dish != null)
        {
            return(
                <FadeTransform
                in
                transformProps={{
                    exitTransform: 'scale(0.5) translateY(-50%)'
                }}>
                <Card>
                    <CardImg top src={baseUrl+dish.image} alt={dish.name}/>
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
                </FadeTransform>
            )
        }else{
            return(
                <div></div>
            )
        }
    }
    function showComment(comment, dishId, postComment, errMess)
        {
          if(errMess){
            return(
                <div className="container">
                    <div className="row">            
                        <h4>{errMess}</h4>
                    </div>
                </div>
            );
          }
       
        else if(comment != null)
        
        {
           const dishComment = comment.map((com) => {
         
            return(
                <div>
                     <Fade in>
                    <ListGroup key={com.id}>
                   
                        <ListGroupItemText>{com.comment}</ListGroupItemText>
                        <ListGroupItemText>--{com.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(com.date)))}</ListGroupItemText>
                    
                    </ListGroup>
                    </Fade>
                </div>
            );
          
        });
           
        return(
            
            
            <div>
            <h4>Comments</h4>
            <Stagger in>
               {dishComment}
               </Stagger>
         {/* Comment Form Class rendered here       */}
               <CommentForm dishId={dishId} postComment={postComment} />
            </div>
        
        );
    }else{
        return(<div></div>)
    }
}
    
  
       const SelectedDish = (props) => {
           if(props.dish!=null)
        return(
            <div className='container'>
                 <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>                
                </div>
            <div className='row'>
                  <div className="col-12 col-md-5 m-1">
                    <RenderDish dish={props.dish} isLoading={props.isLoading} errMess={props.errMess}/>
                    </div>
                  <div className="col-12 col-md-5 m-1">
                   {showComment(props.comments, props.dish.id, props.postComment, props.commentsErrMess)}
                 </div>
            </div>
            </div>    

        );  
        else
        return(
            <div></div>
        )
       } 

export default SelectedDish;
    
        
    