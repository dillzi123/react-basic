import React, {Component} from 'react';
import Menu from './MenuComponent'
import SelectedDish from './SelectedDishComp';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import {postComment, postFeedback, fetchDishes, fetchComments, fetchPromos, fetchLeader} from '../redux/ActionCreators';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import { actions } from 'react-redux-form';
import { TransitionGroup, CSSTransition} from 'react-transition-group';

const mapStateToProps = state => {
    return{
      dishes: state.dishes,
      comments: state.comments,
      promotions: state.promotions,
      leaders: state.leaders
    }
}

const mapDispatchToProps = dispatch =>({
  postcomment:(dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)),
  postfeedback:(firstname, lastname, telnum, email, agree, contactType, message)=>dispatch(postFeedback(firstname, lastname, telnum, email, agree, contactType, message)),
  fetchdishes:()=>{dispatch(fetchDishes())},
  resetFeedbackForm:()=>{dispatch(actions.reset('feedback'))},
  fetchComments:()=>{dispatch(fetchComments())},
  fetchPromos:()=>{dispatch(fetchPromos())},
  fetchLeader:()=>{dispatch(fetchLeader())}  
});

class Main extends Component
{
 
  componentDidMount(){
    this.props.fetchdishes();
    this.props.fetchComments();
    this.props.fetchPromos();
    this.props.fetchLeader();
  }
     
  render(){

  

    const HomePage = ()=> <Home 
    dish = {this.props.dishes.dishes.filter((dish)=>dish.featured)[0]}
    dishesLoading={this.props.dishes.isLoading}
    dishesErrMess={this.props.dishes.errMess}
    promotion = {this.props.promotions.promos.filter((promo)=>promo.featured)[0]}
    promoLoading={this.props.promotions.isLoading}
    promoErrMess={this.props.promotions.errMess}
    leader = {this.props.leaders.leader.filter((leader)=>leader.featured)[0]}
    leaderLoading={this.props.leaders.isLoading}
    leaderErrMess={this.props.leaders.errMess}
    />

    const aboutPage = ()=> <About 
    leaders = {this.props.leaders.leader}
    leaderLoading={this.props.leaders.isLoading}
    leaderErrMess={this.props.leaders.errMess}/> 

 
     
    const DishWithId = ({match})=>
    {
      return(
      <SelectedDish dish = {this.props.dishes.dishes.filter((dish)=>dish.id===parseInt(match.params.dishId,10))[0]}
      isLoading={this.props.dishes.isLoading}
      errMess={this.props.dishes.errMess}
       comments = {this.props.comments.comments.filter((comment)=> comment.dishId===parseInt(match.params.dishId,10))}
       commentsErrMess={this.props.comments.errMess}
       postComment = {this.props.postcomment}/>
      )
    }
    

    return(
        <>
        <Header />
        <TransitionGroup  >
          <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
        <Switch>
              <Route path='/home' component={HomePage} />
              <Route exact path='/menu' component={() => <Menu dishes={this.props.dishes} />} />
              <Route path='/menu/:dishId' component={DishWithId}/>
              <Route exact path='/contactus' component={()=><Contact resetFeedbackForm={this.props.resetFeedbackForm} 
                                                                     postFeedback = {this.props.postfeedback} />} /> 
              <Route exact path='/aboutus' component={aboutPage} />
              <Redirect to="/home" />
        </Switch>
        </CSSTransition>
        </TransitionGroup>
        <Footer />
        </>
   
     
    );
   
  }
  
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
